package com.netflix.application.integration;

import com.netflix.application.holder.AuthenticationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@Component
public class FileIntegration {
    @Value("${netflix.upload-file.endpoint}")
    private String url;
    @Autowired
    private ServiceIntegration serviceIntegration;

    public void uploadFile(File file) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException, URISyntaxException {

        url = url.replace("{user-id}", AuthenticationHolder.getUserId().toString());
        Map<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.AUTHORIZATION, AuthenticationHolder.getTokenId());
        serviceIntegration.uploadFile(url, HttpMethod.POST, headers, null, file);
    }
}
