package com.netflix.application.integration;

import com.netflix.application.dtos.LoginDto;
import com.netflix.application.dtos.MessageRequest;
import com.netflix.application.dtos.MessageResponse;
import com.netflix.application.holder.AuthenticationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@Component
public class MessageIntegration {
    @Autowired
    private ServiceIntegration serviceIntegration;
    @Value("${netflix.message.endpoint}")
    private String messageEndpoint;

    public MessageResponse publishMessage(MessageRequest messageRequest) throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        Map<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.put(HttpHeaders.AUTHORIZATION, AuthenticationHolder.getTokenId());
        return serviceIntegration.integrated(messageEndpoint, HttpMethod.POST, headers, null, messageRequest, null, MessageResponse.class);
    }
}
