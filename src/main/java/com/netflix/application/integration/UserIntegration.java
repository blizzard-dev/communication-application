package com.netflix.application.integration;

import com.netflix.application.dtos.LoginDto;
import com.netflix.application.dtos.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import sun.net.www.HeaderParser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@Component
public class UserIntegration {
    @Autowired
    private ServiceIntegration serviceIntegration;
    @Value("${netflix.authenticate.endpoint}")
    private String authenticateEndpoint;
    @Value("${netflix.user-profile.endpoint}")
    private String getUserProfileEndpoint;

    public LoginDto authenticate(LoginDto loginDto) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException, URISyntaxException {
        Map<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return serviceIntegration.integrated(authenticateEndpoint, HttpMethod.POST, headers, null, loginDto, null, LoginDto.class);
    }

    public UserResponse getUserLoginProfile(String tokenId) throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        Map<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.put(HttpHeaders.AUTHORIZATION, tokenId);
        return serviceIntegration.integrated(getUserProfileEndpoint, HttpMethod.GET, headers, null, null, null, UserResponse.class);
    }

}
