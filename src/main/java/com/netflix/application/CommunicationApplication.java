package com.netflix.application;

import com.netflix.application.config.MqttClientSubscribe;
import com.netflix.application.config.StageReadyEvent;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

public class CommunicationApplication extends Application {
    private ConfigurableApplicationContext applicationContext;

    @Override
    public void start(Stage primaryStage) throws Exception {
        applicationContext.publishEvent(new StageReadyEvent(primaryStage));
        MqttClientSubscribe.connect();
        MqttClientSubscribe.subscribe();
    }

    @Override
    public void init() throws Exception {
        applicationContext = new SpringApplicationBuilder(MainApplication.class).run();
    }

    @Override
    public void stop() throws Exception {
        MqttClientSubscribe.close();
        applicationContext.close();
        Platform.exit();
        System.exit(0);
    }
}
