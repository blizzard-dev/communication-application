package com.netflix.application.controller;

import com.netflix.application.constant.Constant;
import com.netflix.application.dtos.LoginDto;
import com.netflix.application.dtos.UserResponse;
import com.netflix.application.holder.AuthenticationHolder;
import com.netflix.application.integration.MessageIntegration;
import com.netflix.application.integration.UserIntegration;
import com.netflix.application.utils.SchedulerScreenShot;
import io.reactivex.rxjavafx.observables.JavaFxObservable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import lombok.extern.log4j.Log4j2;
import org.apache.http.client.HttpResponseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Timer;

@Component
@Log4j2
public class AuthenticationController {
    @Value("classpath:/view/chat-view.fxml")
    private Resource chatView;
    @FXML
    private Button btnLogin;
    @FXML
    private TextField userNameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label loginMessage;
    @FXML
    private Parent parentMessageView;

    @Autowired
    private UserIntegration userIntegration;
    @Autowired
    private MessageIntegration messageIntegration;
    @Autowired
    private SchedulerScreenShot schedule;

    public AuthenticationController() {
    }

    @FXML
    public void authenticate(ActionEvent actionEvent) throws URISyntaxException, IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        log.info(actionEvent.getEventType().getName());
        String userName = userNameField.getText();
        String password = passwordField.getText();
        LoginDto loginDto = new LoginDto(userName, password);
        try {
            LoginDto authenticate = userIntegration.authenticate(loginDto);
            AuthenticationHolder.setTokenId(authenticate.getTokenId());
            UserResponse userLoginProfile = userIntegration.getUserLoginProfile(authenticate.getTokenId());
            AuthenticationHolder.setUserId(Long.valueOf(userLoginProfile.getUserId()));
            AuthenticationHolder.setUserName(userLoginProfile.getUsername());
            processAfterLogin(userLoginProfile, actionEvent);
        } catch (HttpResponseException e) {
            loginMessage.setTextFill(Color.rgb(255, 0, 0));
            if (e.getStatusCode() == HttpStatus.UNAUTHORIZED.value()) {
                loginMessage.setText("You are not authorized to access this page.");
            } else {
                loginMessage.setText("Something went wrong! Please contact your administrator");
            }
        }
    }

    private void processAfterLogin(UserResponse userLoginProfile, ActionEvent actionEvent) {
        this.startJob();
        if (userLoginProfile != null) {
            String role = userLoginProfile.getRoles().get(0);
            if (Constant.EMPLOYEE.equalsIgnoreCase(role)) {
                this.initMessageView();
                Scene messageViewParent = new Scene(parentMessageView);
                Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
                window.setScene(messageViewParent);
            } else {
                throw new NotImplementedException();
            }
        }
    }

    private void initMessageView() {
        try {
            FXMLLoader loader = new FXMLLoader(chatView.getURL());
            parentMessageView = loader.load();
            MessageController messageController = loader.getController();
            messageController.initMqttCallback();
            messageController.setMessageIntegration(messageIntegration);
        } catch (IOException ex) {
            log.error(ex.getLocalizedMessage(), ex);
        }
    }

    private void startJob() {
        log.info("Job screen shoot starting");
        Timer timer = new Timer();
        timer.schedule(schedule, 0, 5 * 6 * 1000);
    }
}
