package com.netflix.application.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.application.config.MqttClientSubscribe;
import com.netflix.application.constant.Constant;
import com.netflix.application.dtos.MessageRequest;
import com.netflix.application.dtos.MessageResponse;
import com.netflix.application.holder.AuthenticationHolder;
import com.netflix.application.integration.MessageIntegration;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import lombok.extern.log4j.Log4j2;
import org.apache.http.client.HttpResponseException;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
@Log4j2
public class MessageController implements MqttCallback {
    public TextArea messageContent;
    public ScrollPane scrollPane;
    public Button submitBtn;
    private ObjectMapper objectMapper = new ObjectMapper();
    private int index = 0;
    private MessageIntegration messageIntegration;

    public MessageController() {
    }

    public void setMessageIntegration(MessageIntegration messageIntegration) {
        this.messageIntegration = messageIntegration;
    }

    public void initMqttCallback() {
        MqttClientSubscribe.getMqttClientSub().setCallback(this);
    }

    private final VBox chatBox = new VBox(15);

    private final List<Label> messages = new ArrayList<>();

    public void sendMessage(ActionEvent actionEvent) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException, URISyntaxException {
        String content = messageContent.getText();
        messageContent.clear();
        MessageRequest messageRequest = new MessageRequest();
        messageRequest.setMessageContent(content);
        messageRequest.setReportedDate(ZonedDateTime.now().format(Constant.DEFAULT_DATE_TIME_FORMATTER));
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Callable<MessageResponse> callable = () -> messageIntegration.publishMessage(messageRequest);
        try {
            executorService.invokeAny(Arrays.asList(callable));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getLocalizedMessage(), e);
        } catch (ExecutionException e) {
            log.error(e.getLocalizedMessage(), e);
        } finally {
            executorService.shutdown();
        }
    }


    @Override
    public void connectionLost(Throwable throwable) {
        log.error(throwable);
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        log.info("messageArrived of topic: {}", topic);
        MessageResponse messageResponse = objectMapper.readValue(mqttMessage.getPayload(), MessageResponse.class);
        Label label = new Label(messageResponse.getSender() + ": " + messageResponse.getMessageContent() + " " + messageResponse.getReportedDate());
        label.setFont(new Font("Tahoma", 16));
        if (messageResponse.getSender().equals(AuthenticationHolder.getUserName())) {
            label.setTextFill((Color.web("#4287f5")));
        }
        Platform.runLater(() -> {
            messages.add(label);
            scrollPane.setContent(chatBox);
            chatBox.getChildren().add(messages.get(index));
            index++;
        });

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        log.info("DeliveryComplete: {}", iMqttDeliveryToken.getClient().getClientId());
    }
}
