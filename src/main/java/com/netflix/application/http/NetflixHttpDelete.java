package com.netflix.application.http;

import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;

import java.net.URI;

public class NetflixHttpDelete extends HttpEntityEnclosingRequestBase {
    @Override
    public String getMethod() {
        return "DELETE";
    }

    public NetflixHttpDelete(final String uri) {
        super();
        setURI(URI.create(uri));
    }
}
