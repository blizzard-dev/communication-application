package com.netflix.application.config;

import com.fasterxml.uuid.Generators;
import lombok.extern.log4j.Log4j2;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

@Log4j2
public class MqttClientSubscribe {
    private static MqttClient mqttClientSub;
    private static final String URL = "tcp://35.199.77.250:80";
    private static final String TOPIC = "/communication-service";

    public static void connect() throws MqttException {
        if (mqttClientSub == null) {
            mqttClientSub = new MqttClient(URL, Generators.randomBasedGenerator().generate().toString());
        }
        log.info("Prepare to connecting Cloud MQTT");
        mqttClientSub.connect();
        log.info("Connecting Cloud MQTT Successfully");
    }

    public static MqttClient getMqttClientSub() {
        return mqttClientSub;
    }

    public static void close() throws MqttException {
        mqttClientSub.disconnect();
        mqttClientSub.close();
    }

    public static void subscribe() throws MqttException {
        mqttClientSub.subscribe(TOPIC, 0);
    }
}
