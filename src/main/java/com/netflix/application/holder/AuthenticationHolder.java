package com.netflix.application.holder;

public class AuthenticationHolder {
    private static String tokenId;
    private static Long userId;
    private static String userName;

    public static String getTokenId() {
        return tokenId;
    }

    public static void setTokenId(String tokenId) {
        AuthenticationHolder.tokenId = tokenId;
    }

    public static Long getUserId() {
        return userId;
    }

    public static void setUserId(Long userId) {
        AuthenticationHolder.userId = userId;
    }

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        AuthenticationHolder.userName = userName;
    }
}
