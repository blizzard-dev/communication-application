package com.netflix.application.utils;

import com.netflix.application.constant.Constant;
import com.netflix.application.integration.FileIntegration;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.TimerTask;

@Component
@Log4j2
public class SchedulerScreenShot extends TimerTask {
    private static final String FOLDER = System.getProperty("user.home") + File.separator + "PrtScr";
    @Autowired
    private FileIntegration fileIntegration;

    @PostConstruct
    private void init() {
        System.setProperty("java.awt.headless", "false");
    }

    @Override
    public void run() {
        try {
            Robot robot = new Robot();
            String format = "jpg";
            String fileName = FOLDER
                    + File.separator + "SC_" + LocalDateTime.now().format(Constant.DATE_TIME_FORMATTER_NO_ZONE) + "." + format;
            Files.createDirectories(Paths.get(FOLDER));
            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
            File file = new File(fileName);
            ImageIO.write(screenFullImage, format, file);
            fileIntegration.uploadFile(file);
        } catch (AWTException | HeadlessException | IOException | KeyManagementException | NoSuchAlgorithmException | KeyStoreException | URISyntaxException e) {
            log.error(e.getLocalizedMessage(), e);
        }
    }
}
