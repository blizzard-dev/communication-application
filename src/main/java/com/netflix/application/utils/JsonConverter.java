package com.netflix.application.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Log4j2
public class JsonConverter {

    public <T> T convertJsonToObject(String json, Class<T> type) {
        ObjectMapper objectMapper = new ObjectMapper();
        T t = null;
        try {
            t = objectMapper.readValue(json, type);
        } catch (IOException e) {
            log.error("Parse failed", e);
        }
        return t;
    }
}
