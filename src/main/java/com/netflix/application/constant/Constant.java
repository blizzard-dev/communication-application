package com.netflix.application.constant;

import java.time.format.DateTimeFormatter;

public class Constant {
    public static final String MANAGER = "MANAGER";
    public static final String EMPLOYEE = "EMPLOYEE";
    public static final DateTimeFormatter DEFAULT_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");
    public static final DateTimeFormatter DATE_TIME_FORMATTER_NO_ZONE = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH-mm-ss");
}
